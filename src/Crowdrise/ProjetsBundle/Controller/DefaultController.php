<?php

namespace Crowdrise\ProjetsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function projectsAction()
    {
        return $this->render('CrowdriseProjetsBundle:Default:some_projects.html.twig');
    }
    
    public function allprojectsAction()
    {
        return $this->render('CrowdriseProjetsBundle:Default:all_projects.html.twig');
    }
    
}
